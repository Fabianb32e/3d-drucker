# 3D Drucker

## Getting started

This project is a console application, that simulates a 3D Printer.

- There is a menu shown in the console.
- You as a user can control the 3D Printer with some user inputs.
- The 3D Printer will output information, due to the user inputs.

## Project structure

In the project, there are two main folders:

- source → contains all the source files
- structure → contains the Nassi-Shneiderman-Diagramm
- requirements → contains the requirements from the functionalities
- testing → contains the test process that tests all the functionalities of the software
- print_files → contains files that were used to test the application
- exe → contains two types of exe
  - exe that clears the terminal
  - exe that doesn't clear the terminal

## Releases

- Every big release will have a tag with a version number to save the state.

## Project status

- current state: "under heavy construction!"
