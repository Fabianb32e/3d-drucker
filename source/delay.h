//-------------------------------------------------------------------------
//  Author:         Fabian Bachofen
//
//  Project Name:   3D-Drucker Software
//  File Name:      delay.h
//  Version:        1.00
//-------------------------------------------------------------------------

#ifndef DELAY_H
#define DELAY_H

//--- Libraries -----------------------------------------------------------
#include "global.h"

//--- Defines -------------------------------------------------------------
#define     ALLOW_DELAY         TRUE

//--- Function Declarations -----------------------------------------------
void delay      (int delay_time_ms);        // function to delay parts of the printing process

#endif // DELAY_H

// <-- end of file
