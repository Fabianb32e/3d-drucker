//-------------------------------------------------------------------------
//  Author:         Fabian Bachofen
//
//  Project Name:   3D-Drucker Software
//  File Name:      global.h
//  Version:        1.00
//-------------------------------------------------------------------------

#ifndef GLOBAL_H
#define GLOBAL_H

//--- Libraries -----------------------------------------------------------

//--- standart libraries ----------------------------------------------
#include <stdio.h>
#include <stdlib.h>

#include <math.h>
#include <string.h>
#include <time.h>
#include <dirent.h>

//--- own made libraries ----------------------------------------------
#include "output.h"
#include "delay.h"
#include "printer.h"
#include "input.h"
#include "file.h"

//--- Defines -------------------------------------------------------------
#define     TRUE                    1
#define     FALSE                   0

//--- Informations ----------------------------------------------------
#define     AUTHOR                  "Fabian Bachofen"
#define     PROJECT                 "3D-Printer"
#define     VERSION                 "1.0"

//--- Printer Menu ----------------------------------------------------
enum menu  {FILE_SEARCH     = 1,
            START_PRINT     = 2,
            PREHEAT         = 3,
            CHANGE_SPOOL    = 4,
            INFORMATION     = 5,
            END_PROGRAM     = 6};

//--- Printer Settings ------------------------------------------------
#define     MIN_PREHEAT_TEMP               60   // in deg C
#define     MAX_PREHEAT_TEMP              300   // in deg C

#define     TEMP_STEP_PREHEAT               1   // in deg C per Second
#define     TEMP_STEP_CHANGE_FILAMENT       5   // in deg C per Second
#define     TEMP_STEP_PRINT                 5   // in deg C per Second

#define     TIME_UNLOAD_FILAMENT         5000   // in ms
#define     TIME_LOAD_FILAMENT           5000   // in ms

#define     TEMP_ROOM                      20   // in deg C
#define     TEMP_UNLOAD_FILAMENT          240   // in deg C

#endif // GLOBAL_H

// <-- end of file
