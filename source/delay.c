//-------------------------------------------------------------------------
//  Author:         Fabian Bachofen
//
//  Project Name:   3D-Drucker Software
//  File Name:      delay.c
//  Version:        1.00
//-------------------------------------------------------------------------

//--- Libraries -----------------------------------------------------------
#include "delay.h"

//--- Functions -----------------------------------------------------------

//--- Delay -----------------------------------------------------------
void delay(int delay_time_ms)
{
    clock_t start_time = clock();

    while(clock() < start_time + delay_time_ms);
}

// <-- end of file
