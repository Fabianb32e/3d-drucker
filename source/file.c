//-------------------------------------------------------------------------
//  Author:         Fabian Bachofen
//
//  Project Name:   3D-Drucker Software
//  File Name:      file.c
//  Version:        1.00
//-------------------------------------------------------------------------

//--- Libraries -----------------------------------------------------------
#include "file.h"

//--- Variables -----------------------------------------------------------
const char  valid_file_type[][4]    = {".g", ".G", ".gx", ".gX", ".Gx", ".GX"};
const int   file_type_count         = sizeof(valid_file_type) / sizeof(valid_file_type[0]);

//--- Functions -----------------------------------------------------------

//--- List the Directory ----------------------------------------------
int list_directory(const char *path, char file_name[MAX_FILES][MAX_FILE_NAME])
{
    int loop_cnt    = 0;

    struct dirent *de;

    DIR *dir = opendir(path);               // open a directory

    if(dir == NULL)                         // check if directory exists
    {
        print_no_directory();               // print message, that directory doesn't exist

        getchar();                          // get enter to go on
        fflush(stdin);                      // clear input buffer
        return 0;
    }

    int i = 0;

    while ((de = readdir(dir)) != NULL)                 // read all the files, until all the files have been scanned
    {
        char *filename = de -> d_name;
        size_t length = strlen(filename);

        for (i = 0; i < file_type_count; i++)
        {
            size_t lengthsuffix = strlen(valid_file_type[i]);

            if (lengthsuffix <= length)
            {
                int compare_result = strcmp(filename + (length - lengthsuffix), valid_file_type[i]);        // check if the file type is allowed

                //--- file type is valid
                if (compare_result == 0)
                {
                    print_directory_file(loop_cnt+1, filename);         // print the filename to the console

                    strcpy(file_name[loop_cnt], filename);              // save the filename in an array

                    loop_cnt++;
                    break;
                }
            }
        }
    }

    closedir(dir);                  // close the opened directory

    print_file_search_end();        // print message, that the file search is finished.

    return loop_cnt;                // return the number of files found in the directory, that have the right filetype
}

//--- Open file -------------------------------------------------------
struct PrintFile open_file(char *path,  char* filename)
{
    struct PrintFile print_file;
    struct PrintFile empty_print_file = {.duration    = 0,
                                         .temperature = 0,
                                         .shape       = 0,
                                         .length      = 0};

    char full_path[256];

    //--- create the full file path
    strcpy(full_path, path);
    strcat(full_path, "/");
    strcat(full_path, filename);

    FILE* file = fopen(full_path, "r");     // open the file

    if(file == NULL)                        // check if file exists
    {
        print_error_file();                 // print message, that the printfile doesn't exist

        getchar();                          // get enter to go on
        fflush(stdin);                      // clear input buffer
        return empty_print_file;
    }

    fscanf(file, "%d;%d", &print_file.duration, &print_file.temperature);       // get print duration and print temperature from file

    int i;

    // get all the shape data
    for(i = 0; i < 100; i++)
    {
        if(fgets(print_file.shape[i], sizeof(print_file.shape[i]), file) == NULL)
        {
            break;
        }

        char * newline = strchr(print_file.shape[i], '\n');         // Remove the newline character if present

        if(newline != NULL)
        {
            * newline = '\0';
        }
    }

    print_file.length = i - 1;          // save the length of the shape

    fclose(file);                       // close the file

    return print_file;                  // return a struct, that contains the printfile
}

//--- Check Printfile Data --------------------------------------------
int check_printfile(struct PrintFile printfile)
{
    //--- Duration of less or equal to 0 is bad
    if(printfile.duration <= 0) return 0;

    //--- Temperature of less or equal to 0 is bad
    if(printfile.temperature <= 0) return 0;

    //--- length of less or equal to 0 is bad
    if(printfile.length <= 0) return 0;

    return 1;
}

// <-- end of file
