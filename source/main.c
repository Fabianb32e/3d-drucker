//-------------------------------------------------------------------------
//  Author:         Fabian Bachofen
//
//  Project Name:   3D-Drucker Software
//  File Name:      main.c
//  Version:        1.00
//-------------------------------------------------------------------------

//--- Instructions --------------------------------------------------------
//
//  Allow that the terminal gets cleared:
//  - If the software gets executed directly from the IDE:
//      - change the ALLOW_CLEAR_TERMINAL to FALSE in outputs.h
//  - Otherwise, when it gets executed as an .exe -file
//      - change the ALLOW_CLEAR_TERMINAL to TRUE  in outputs.h
//  If this setting is set wrong, it can result in wrong terminal outputs!
//
//
//  Different directory path for the print files:
//  - change the DIRECTORY_PATH to the new path in file.h
//
//-------------------------------------------------------------------------

//--- Libraries -----------------------------------------------------------
#include <stdio.h>
#include "global.h"

//--- Main Program --------------------------------------------------------
int main(void)
{
    //--- Initialization ----------------------------------------------

    //--- Local Variables -----------------------------------------
    int         program_run     = 1;
    int         user_input      = 0;

    //--- Program Loop ------------------------------------------------
    while(program_run == 1)
    {
        //--- Program Menu and User Input -------------------------
        print_printer_menu();                       // print the printer menu
        user_input = menu_input();                  // Get the user input

        //--- Printer Functions -----------------------------------
        switch(user_input)
        {
            //--- File Serach ---------------------------------
            case FILE_SEARCH:
                search_file();                      // Function to search for a print file and start to print, if the user want's to start
                break;

            //--- Start Print ---------------------------------
            case START_PRINT:
                print();                            // Function to start printing
                break;

            //--- Preheat Printer -----------------------------
            case PREHEAT:
                preheating_printer();               // Function to preheat the 3D-Printer
                break;

            //--- Change Spool --------------------------------
            case CHANGE_SPOOL:
                changing_spool_printer();           // Function to change the filament spool
                break;

            //--- Information ---------------------------------
            case INFORMATION:
                print_printer_information();        // print the Printer Informations
                getchar();                          // get enter to go on
                fflush(stdin);                      // clear input buffer
                break;

            //--- End Program ---------------------------------
            case END_PROGRAM:
                print_printer_shutdown();           // print the shutdown message
                program_run = 0;                    // stop program
                break;

            default:
                print_input_not_valid();            // print message, that the input was not valid
                break;
        } // <-- switch
    } // <-- while

    return 0;
} // <-- Main

// <-- end of file
