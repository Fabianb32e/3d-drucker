//-------------------------------------------------------------------------
//  Author:         Fabian Bachofen
//
//  Project Name:   3D-Drucker Software
//  File Name:      output.h
//  Version:        1.00
//-------------------------------------------------------------------------

#ifndef OUTPUT_H
#define OUTPUT_H

//--- Libraries -----------------------------------------------------------
#include "global.h"

//--- Defines -------------------------------------------------------------
#define     ALLOW_CLEAR_TERMINAL        FALSE

//--- Platform Check --------------------------------------------------
#ifdef _WIN32
    #define PLATFORM_NAME  "windows"
    #define TERMINAL_CLEAR "cls"
#elif __MACH__
    #define PLATFORM_NAME  "mac"
    #define TERMINAL_CLEAR "clear"
#elif __linux__
    #define PLATFORM_NAME  "linux"
    #define TERMINAL_CLEAR "clear"
#endif

//--- Functions -----------------------------------------------------------
void clear_terminal                     (void);                             // function to clear the terminal / console
void print_printer_menu                 (void);                             // function to display the printer menu
void print_printer_preheat              (void);                             // function to display the preheat temperature selection
void print_printer_preheat_temp         (int actual_temp, int set_temp);    // function to display the heating process
void print_printer_preheat_finished     (void);                             // function to display the preheating has finished
void print_printer_open_lid             (void);                             // function to display to open the lid of the printer
void print_printer_turn_spool_cw        (void);                             // function to display that the spool is turning to the right side
void print_printer_turn_spool_ccw       (void);                             // function to display that the spool is turning to the left  side
void print_printer_remove_spool         (void);                             // function to display to remove the spool
void print_printer_cool_down_temp       (int actual_temp, int set_temp);    // function to display the progress of the cooldown
void print_printer_information          (void);                             // function to display the printer informations
void print_printer_shutdown             (void);                             // function to display that the printer will shutdown
void print_input_not_valid              (void);                             // function to display that the input is not valid
void print_input_out_of_range           (void);                             // function to display that the input is out of range
void print_spool_change_finished        (void);                             // function to display that the filament change is finished
void print_spool_change_abort           (void);                             // function to display that the filament changes was aborted
void print_no_directory                 (void);                             // function to display that the directory doesn't exist
void print_error_file                   (void);                             // function to display that the print file doesn't exist
void print_empty_directory              (void);                             // function to display that the directory is empty
void print_file_search                  (void);                             // function to display the file search menu
void print_directory_file               (int file_number, char * filename); // function to display a file in the directory for the file search option
void print_file_search_end              (void);                             // function to display the end of the file search
void print_select_file                  (void);                             // function to display to select a print file
void print_select_file_abort            (void);                             // function to display that the selection of a print file was aborted
void print_printfile_corrupted          (void);                             // function to display that the print file is corrupted
void print_printfile_loaded             (void);                             // function to display that the print file is loaded
void print_printfile_not_loaded         (void);                             // function to display that the print file is not loaded
void print_start_print                  (void);                             // function to display that the print has started
void print_heating_finished             (int temperature);                  // function to display that the heating is finished
void print_estimated_print_time         (int printtime_ms);                 // function to display the estimated print time of the print
void print_turn_spool_cw                (void);                             // function to display that the spool turns to the right side
void print_start_printing_shape         (void);                             // function to display the start of the print
void print_finished_print               (void);                             // function to display that the print has finished
void print_continue_printing            (void);                             // function to display to continue to the print from the print file selection

#endif // OUTPUT_H

// <-- end of file
