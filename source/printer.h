//-------------------------------------------------------------------------
//  Author:         Fabian Bachofen
//
//  Project Name:   3D-Drucker Software
//  File Name:      printer.h
//  Version:        1.00
//-------------------------------------------------------------------------

#ifndef PRINTER_H
#define PRINTER_H

//--- Libraries -----------------------------------------------------------
#include "global.h"

//--- Defines -------------------------------------------------------------

#define     DELAY_HEATING       1000        // delay time for the heating function
#define     DELAY_COOLING       1000        // delay time for the cooling function

enum spool_direction           {LEFT  = 1,
                                RIGHT = 2};

//--- Function Declarations -----------------------------------------------
void heating                    (int actual_temp, int set_temp, int step_temp);     // function to heat up the printer
void cool_down                  (int actual_temp, int set_temp, int step_temp);     // function to cool down the printer
void turn_spool                 (int direction, int time);                          // function to turn the spool for a certain time

void preheating_printer         (void);     // function to preheat the printer
void changing_spool_printer     (void);     // function to change the spool

void search_file                (void);     // function to serach and select a print file
void print                      (void);     // function to print the print file

#endif // PRINTER_H

// <-- end of file
