//-------------------------------------------------------------------------
//  Author:         Fabian Bachofen
//
//  Project Name:   3D-Drucker Software
//  File Name:      file.h
//  Version:        1.00
//-------------------------------------------------------------------------

#ifndef FILE_H
#define FILE_H

//--- Libraries -----------------------------------------------------------
#include "global.h"

//--- Defines -------------------------------------------------------------
#define     MAX_FILES               255
#define     MAX_FILE_NAME           30
#define     DIRECTORY_PATH          "../print_files"

#define     MAX_SHAPE_SIZE          100

//--- Structs -------------------------------------------------------------
struct PrintFile
{
    int     duration;
    int     temperature;
    char    shape[MAX_SHAPE_SIZE][MAX_SHAPE_SIZE];
    int     length;
};

//--- Functions ----------------------------------------------------------
int                 list_directory      (const char *path, char file_name[MAX_FILES][MAX_FILE_NAME]);       // function to list all the print files in the directory
struct PrintFile    open_file           (char *path,  char* filename);                                      // function to open and read the print file
int                 check_printfile     (struct PrintFile printfile);                                       // function to check the print file

#endif // FILE_H

// <-- end of file
