//-------------------------------------------------------------------------
//  Author:         Fabian Bachofen
//
//  Project Name:   3D-Drucker Software
//  File Name:      delay.c
//  Version:        1.00
//-------------------------------------------------------------------------

//--- Libraries -----------------------------------------------------------
#include "printer.h"

//--- Variables and Arrays ------------------------------------------------
struct PrintFile selected_printfile;        // struct where the file print data get copied

int printfile_loaded = 0;                   // variable as a flag for the state if a printfile is loaded or not

//--- Functions -----------------------------------------------------------

//--- Printer Heating -------------------------------------------------
void heating(int actual_temp, int set_temp, int step_temp)
{
    for(; actual_temp <= set_temp; actual_temp += step_temp)
    {
        if(ALLOW_DELAY == 1)
        {
            delay(DELAY_HEATING);
        }

        print_printer_preheat_temp(actual_temp, set_temp);
    }
}

//--- Printer Cool-down -----------------------------------------------
void cool_down(int actual_temp, int set_temp, int step_temp)
{
    for(; actual_temp >= set_temp; actual_temp -= step_temp)
    {
        if(ALLOW_DELAY == 1)
        {
            delay(DELAY_COOLING);
        }

        print_printer_cool_down_temp(actual_temp, set_temp);
    }
}

//--- Printer turn spool ----------------------------------------------
void turn_spool(int direction, int time)
{
    switch(direction)
    {
        case LEFT:
            print_printer_turn_spool_ccw();
            break;

        case RIGHT:
            print_printer_turn_spool_cw();
            break;
    }

    if((time > 0) && (ALLOW_DELAY == 1))
    {
        delay(time);
    }
}

//--- Printer preheating ----------------------------------------------
void preheating_printer(void)
{
    int preheat_temperature = 0;
    int preheat_abort       = 0;

    print_printer_preheat();                                        // print the Printer Preheat Menu
    preheat_abort = preheat_input(& preheat_temperature);           // get user input for Preheating

    if(preheat_abort == 1) return;                                  // abort preheating

    heating(TEMP_ROOM, preheat_temperature, TEMP_STEP_PREHEAT);     // heating the printer

    print_printer_preheat_finished();                               // print preheating finished

    getchar();                                                      // get enter to go on
    fflush(stdin);                                                  // clear input buffer
}

//--- Printer changing spool ------------------------------------------
void changing_spool_printer(void)
{
    int user_input_valid = 0;

    print_printer_open_lid();                       // Function to show instruction in the terminal

    user_input_valid = change_filament_input();     // get the user input

    if(user_input_valid != 1)                       // check the user input
    {
        print_spool_change_abort();                 // message, that the spool change has aborted
        getchar();                                  // get enter to go on
        fflush(stdin);                              // clear input buffer

        return;                                     // abort changing the spool
    }

    heating(TEMP_ROOM, TEMP_UNLOAD_FILAMENT, TEMP_STEP_CHANGE_FILAMENT);            // heat up to certain temperature
    turn_spool(LEFT, TIME_UNLOAD_FILAMENT);                                         // turn the spool
    print_printer_remove_spool();                                                   // message, that the spool can be removed

    user_input_valid = change_filament_input();                                     // get the user input

    if(user_input_valid == 1)                                                       // check the user input
    {
        turn_spool(RIGHT, TIME_LOAD_FILAMENT);                                      // turn the spool
        cool_down(TEMP_UNLOAD_FILAMENT, TEMP_ROOM, TEMP_STEP_CHANGE_FILAMENT);      // cool down the room temperature
        print_spool_change_finished();                                              // message, that the spool change has finished
    }
    else
    {
        cool_down(TEMP_UNLOAD_FILAMENT, TEMP_ROOM, TEMP_STEP_CHANGE_FILAMENT);      // cool down the room temperature
        print_spool_change_abort();                                                 // message, that the spool change has aborted
    }

    getchar();          // get enter to go on
    fflush(stdin);      // clear input buffer
}

//--- Serach File -----------------------------------------------------
void search_file(void)
{
    // create a files list, that has the string "null\n" inside
    char files_list[MAX_FILES][MAX_FILE_NAME] = {
            { 'n', 'u', 'l', 'l', '\0' }
    };

    int number_of_files     = 0;
    int file_number         = 0;
    int printfile_okey      = 0;

    int user_input          = 0;

    int file_search_abort   = 0;

    print_file_search();

    number_of_files = list_directory(DIRECTORY_PATH, files_list);               // list the directory

    if(strcmp(files_list[0], "null") == 0)                                      // check if the directory is empty
    {
        print_empty_directory();                                                // print message for empty directory

        getchar();                                                              // get enter to go on
        fflush(stdin);                                                          // clear input buffer
        return;
    }

    print_select_file();                                                        // print message to select file

    file_search_abort = select_file_input(number_of_files, & file_number);      // get the user input for the file selection

    if(file_search_abort == 1)
    {
        print_select_file_abort();

        getchar();                                                              // get enter to go on
        fflush(stdin);                                                          // clear input buffer
        return;
    }

    printfile_loaded = 0;                                                       // reset the flag, that no file is loaded

    char *selected_file = files_list[file_number - 1];                          // pointer to the first filename

    selected_printfile = open_file(DIRECTORY_PATH, selected_file);              // open file and read values and return struct with the data

    printfile_okey = check_printfile(selected_printfile);                       // check if the printfile is valid

    if(printfile_okey == 0)
    {
        print_printfile_corrupted();                // message, that the printfile is corrupted

        getchar();                                  // get enter to go on
        fflush(stdin);                              // clear input buffer
        return;
    }
    else
    {
        print_printfile_loaded();                   // print message that the selected file has been loaded
        printfile_loaded = 1;
    }

    print_continue_printing();                      // print message for continuing with print

    user_input = continue_print_input();            // get user input

    if(user_input == 0) return;

    print();        // start to print the printfile
}

//--- Printing --------------------------------------------------------
void print(void)
{
    if(printfile_loaded == 0)                   // check if printfile is not loaded
    {
        print_printfile_not_loaded();           // print message that no printfile is loaded
        search_file();                          // open search_file function
    }
    else
    {
        int time_per_layer = selected_printfile.duration / selected_printfile.length;       // calculate the time per layer

        print_start_print();                                                                // print message that the print has started

        heating(TEMP_ROOM, selected_printfile.temperature, TEMP_STEP_PRINT);                // heating up

        print_heating_finished(selected_printfile.temperature);                             // print message that the heating is finished

        print_estimated_print_time(selected_printfile.duration);                            // print message on how long the print has to get printed out

        print_turn_spool_cw();                                                              // print that the spool turns clock wise (right)

        print_start_printing_shape();                                                       // print message, that the print of the shape has started

        for(int i = 1; i <= selected_printfile.length; i++)         // for loop to print all the layers
        {
            if(ALLOW_DELAY == 1)                                    // Check if delay is allowed
            {
                delay(time_per_layer);                              // wait until the time for each layer is over
            }
            printf("| %s\n", selected_printfile.shape[i]);          // print the shape of the corresponding layer from the loop
        }

        print_finished_print();                 // print message that the print is finished

        getchar();                              // get enter to go on
        fflush(stdin);                          // clear input buffer
    }
}

// <-- end of file
