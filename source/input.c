//-------------------------------------------------------------------------
//  Author:         Fabian Bachofen
//
//  Project Name:   3D-Drucker Software
//  File Name:      input.c
//  Version:        1.00
//-------------------------------------------------------------------------

//--- Libraries -----------------------------------------------------------
#include "input.h"

//--- Functions -----------------------------------------------------------

//--- Menu Input ------------------------------------------------------
int menu_input(void)
{
    int user_input_valid    = 0;
    int user_input          = 0;

    while(user_input_valid <= 0)                        // while user input is not valid
    {
        user_input_valid = scanf("%d", & user_input);   // read user input
        fflush(stdin);                                  // clear input buffer

        if(user_input > END_PROGRAM)                    // user input validation
        {
            user_input_valid = 0;                       // user input is out of range
        }

        if(user_input_valid <= 0)                       // user input is not
        {
            print_input_not_valid();                    // message, that the input is not valid
        }
    }

    return user_input;
}

//--- Preheat Input ---------------------------------------------------
int preheat_input(int * temperature)
{
    int user_input_valid    = 0;
    int user_input          [10];

    while(user_input_valid <= 0)                                    // while user input is not valid
    {
        scanf("%s", & user_input);                                  // read user input
        fflush(stdin);                                              // clear input buffer

        if(strstr(user_input, "a") || strstr(user_input, "A"))      // check for stop condition
        {
            user_input_valid = 1;                                   // valid input
            return 1;                                               // will stop preheat
        }
        else
        {
            * temperature = atoi(user_input);                       // convert ascii dezimal sting to integer

            if((* temperature < MIN_PREHEAT_TEMP) || (* temperature > MAX_PREHEAT_TEMP))      // check user selected temperature
            {
                print_input_out_of_range();                         // print message out of range
                user_input_valid = 0;                               // input is not valid
            }
            else
            {
                user_input_valid = 1;                               // input is valid
                return 0;                                           // will start preheat
            }
        }
    }

    return 1;       // just for safety (will stop preheat)
}

//--- Change Filament Input -------------------------------------------
int change_filament_input(void)
{
    int     user_input_valid    = 0;
    char    user_input_str      [10];

    while(user_input_valid <= 0)                            // while user input is not valid
    {
        scanf("%s", &user_input_str);                       // read user input
        fflush(stdin);                                      // clear input buffer

        //--- Check for confirmation word
        if((strcmp(user_input_str, OK_1) == 0) ||
           (strcmp(user_input_str, OK_2) == 0) ||
           (strcmp(user_input_str, OK_3) == 0))
        {
            user_input_valid = 1;
            break;
        }

        //--- Check for abort word
        else if((strcmp(user_input_str, ABORT_1) == 0) ||
                (strcmp(user_input_str, ABORT_2) == 0))
        {
            user_input_valid = 0;
            break;
        }

        else
        {
            user_input_valid = 0;                           // input not valid
            print_input_not_valid();                        // print message, that the input is not valid
        }
    }

    return user_input_valid;
}

int select_file_input(int number_of_files, int * file_number)
{
    int user_input_valid    = 0;
    int user_input          [10];

    while(user_input_valid <= 0)                                    // while user input is not valid
    {
        scanf("%s", & user_input);                                  // read user input
        fflush(stdin);                                              // clear input buffer

        if(strstr(user_input, "a") || strstr(user_input, "A"))      // check for stop condition
        {
            user_input_valid = 1;                                   // valid input
            return 1;                                               // will stop file selection
        }

        * file_number = atoi(user_input);                           // convert ascii dezimal sting to integer

        if((* file_number > 0) && (* file_number <= number_of_files))
        {
            user_input_valid = 1;                                   // valid input
            return 0;                                               // will continue file selection
        }
        else
        {
            user_input_valid = 0;                                   // input not valid
            print_input_not_valid();                                // print message, that the input is not valid
        }
    }

    return 1;       // just for safety (will stop file selection)
}

int continue_print_input(void)
{
    int     user_input_valid    = 0;
    char    user_input_str      [10];

    while(user_input_valid <= 0)             // while user input is not valid
    {
        scanf("%s", &user_input_str);        // read user input
        fflush(stdin);                       // clear input buffer

        //--- Check for confirmation word
        if((strcmp(user_input_str, OK_1) == 0) ||
           (strcmp(user_input_str, OK_2) == 0) ||
           (strcmp(user_input_str, OK_3) == 0))
        {
            user_input_valid = 1;
            break;
        }

            //--- Check for abort word
        else if((strcmp(user_input_str, ABORT_1) == 0) ||
                (strcmp(user_input_str, ABORT_2) == 0))
        {
            user_input_valid = 0;
            break;
        }

        else
        {
            user_input_valid = 0;                               // input not valid
            print_input_not_valid();                            // print message, that the input is not valid
        }
    }

    return user_input_valid;
}

// <-- end of file
