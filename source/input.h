//-------------------------------------------------------------------------
//  Author:         Fabian Bachofen
//
//  Project Name:   3D-Drucker Software
//  File Name:      input.h
//  Version:        1.00
//-------------------------------------------------------------------------

#ifndef INPUT_H
#define INPUT_H

//--- Libraries -----------------------------------------------------------
#include "global.h"

//--- Defines -------------------------------------------------------------
#define     OK_1            "ok"
#define     OK_2            "Ok"
#define     OK_3            "OK"

#define     ABORT_1         "a"
#define     ABORT_2         "A"

//--- Functions -----------------------------------------------------------
int menu_input              (void);                                     // function to get the user input for the printer menu
int preheat_input           (int * temperature);                        // function to get the user input for the preheating temperature
int change_filament_input   (void);                                     // function to get the user input to change the filament
int select_file_input       (int number_of_files, int * file_number);   // function to get the user input for the print file selection
int continue_print_input    (void);                                     // function to get the user input to continue frim the printfile selection to the print of it

#endif //INPUT_H

// <-- end of file
