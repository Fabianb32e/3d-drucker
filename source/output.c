//-------------------------------------------------------------------------
//  Author:         Fabian Bachofen
//
//  Project Name:   3D-Drucker Software
//  File Name:      output.c
//  Version:        1.00
//-------------------------------------------------------------------------

//--- Libraries -----------------------------------------------------------
#include "output.h"

//--- Functions -----------------------------------------------------------

//--- Clear Terminal --------------------------------------------------
void clear_terminal(void)
{
    if (ALLOW_CLEAR_TERMINAL == 1)
    {
        system(TERMINAL_CLEAR);         // clears the terminal with a command that is defined by the OS
    }
}

//--- Output Printer Menu ---------------------------------------------
void print_printer_menu(void)
{
    clear_terminal();

    printf("+------------------------------------------------\n");
    printf("|   Flashforge 3D-Printer\n");
    printf("|\n");
    printf("|   Funktions-Menu:\n");
    printf("|\n");
    printf("|  < 1 >    File Suchen\n");
    printf("|  < 2 >    Druck Starten\n");
    printf("|  < 3 >    Vorheizen\n");
    printf("|  < 4 >    Filament-Spule wechseln\n");
    printf("|  < 5 >    Information\n");
    printf("|  < 6 >    Drucker beenden\n");
    printf("|\n");
    printf("+------------------------------------------------\n");
}

//--- Output Printer Preheat Menu -------------------------------------
void print_printer_preheat(void)
{
    clear_terminal();

    printf("+------------------------------------------------\n");
    printf("|   Flashforge 3D-Printer\n");
    printf("|\n");
    printf("|   Heizen:\n");
    printf("|\n");
    printf("|   Waehlen Sie eine Temeperatur\n");
    printf("|   zwischen: %d und %d [Grad Celsius] aus.\n", MIN_PREHEAT_TEMP, MAX_PREHEAT_TEMP);
    printf("|\n");
    printf("|   Oder Brechen Sie mit \'a\' ab.\n");
    printf("|\n");
    printf("+------------------------------------------------\n");
}

//--- Output Printer Preheat Temperature ------------------------------
void print_printer_preheat_temp(int actual_temp, int set_temp)
{
    clear_terminal();

    printf("+------------------------------------------------\n");
    printf("|   Flashforge 3D-Printer\n");
    printf("|\n");
    printf("|   Heizen:\n");
    printf("|\n");
    printf("|   Temperatur: %d / %d [Grad Celsius]\n", actual_temp, set_temp);
    printf("|\n");
    printf("+------------------------------------------------\n");
}

//--- Output Printer Preheat finished ---------------------------------
void print_printer_preheat_finished(void)
{
    clear_terminal();

    printf("+------------------------------------------------\n");
    printf("|   Flashforge 3D-Printer\n");
    printf("|\n");
    printf("|   Vorheizen:\n");
    printf("|\n");
    printf("|   Das Vorheizen ist abgeschlossen.\n");
    printf("|\n");
    printf("|   Druecke Enter um fortzufahren!\n");
    printf("|\n");
    printf("+------------------------------------------------\n");
}

//--- Output Printer Open the Lid -------------------------------------
void print_printer_open_lid(void)
{
    clear_terminal();

    printf("+------------------------------------------------\n");
    printf("|   Flashforge 3D-Printer\n");
    printf("|\n");
    printf("|   Filament Wechsel:\n");
    printf("|\n");
    printf("|   Oeffnen Sie bitte den Deckel und\n");
    printf("|   bestaetigen Sie dies mit \'ok\'.\n");
    printf("|\n");
    printf("|   Oder brechen Sie mit ab mit \'a\'.\n");
    printf("|\n");
    printf("+------------------------------------------------\n");
}

void print_printer_turn_spool_cw(void)
{
    clear_terminal();

    printf("+------------------------------------------------\n");
    printf("|   Flashforge 3D-Printer\n");
    printf("|\n");
    printf("|   Spule dreht im Uhrzeigersin...\n");
    printf("|\n");
    printf("+------------------------------------------------\n");
}

void print_printer_turn_spool_ccw(void)
{
    clear_terminal();

    printf("+------------------------------------------------\n");
    printf("|   Flashforge 3D-Printer\n");
    printf("|\n");
    printf("|   Spule dreht im Gegenuhrzeigersin...\n");
    printf("|\n");
    printf("+------------------------------------------------\n");
}

void print_printer_remove_spool(void)
{
    clear_terminal();

    printf("+------------------------------------------------\n");
    printf("|   Flashforge 3D-Printer\n");
    printf("|\n");
    printf("|   Filament Wechsel:\n");
    printf("|\n");
    printf("|   Die Spule darf nun entfernt werden.\n");
    printf("|\n");
    printf("|   Bestaetigen Sie dies nach Abschluss\n");
    printf("|   mit einem \'ok\'.\n");
    printf("|\n");
    printf("|   Oder brechen Sie mit ab mit \'a\'.\n");
    printf("|\n");
    printf("+------------------------------------------------\n");
}

void print_printer_cool_down_temp(int actual_temp, int set_temp)
{
    clear_terminal();

    printf("+------------------------------------------------\n");
    printf("|   Flashforge 3D-Printer\n");
    printf("|\n");
    printf("|   Filament Wechsel:\n");
    printf("|\n");
    printf("|   Temperatur: %d / %d [Grad Celsius]\n", actual_temp, set_temp);
    printf("|\n");
    printf("+------------------------------------------------\n");
}

//--- Output Printer Information --------------------------------------
void print_printer_information(void)
{
    clear_terminal();

    printf("+------------------------------------------------\n");
    printf("|   Flashforge 3D-Printer\n");
    printf("|\n");
    printf("|   Informationen:\n");
    printf("|\n");
    printf("|   Software Author: %s\n", AUTHOR);
    printf("|   Project Name:    %s\n", PROJECT);
    printf("|   Software Version %s\n", VERSION);
    printf("|\n");
    printf("|\n");
    printf("|   Druecken Sie Enter um ins Menu\n");
    printf("|   zurueckzukehren.\n");
    printf("|\n");
    printf("+------------------------------------------------\n");
}

//--- Output Printer Shutdown -----------------------------------------
void print_printer_shutdown(void)
{
    clear_terminal();

    printf("+------------------------------------------------\n");
    printf("|   Flashforge 3D-Printer\n");
    printf("|\n");
    printf("|   3D-Drucker wird ausgeschaltet...\n");
    printf("|\n");
    printf("+------------------------------------------------\n");
}

//--- Output Input not Valid ------------------------------------------
void print_input_not_valid(void)
{
    printf("+------------------------------------------------\n");
    printf("|\n");
    printf("|   Die Eingabe ist ungueltig!\n");
    printf("|\n");
    printf("+------------------------------------------------\n");
}

//--- Output Input out of range ---------------------------------------
void print_input_out_of_range(void)
{
    printf("+------------------------------------------------\n");
    printf("|\n");
    printf("|   Die Eingabe ist ausserhalb des\n");
    printf("|   erlaubten Bereichs!\n");
    printf("|\n");
    printf("+------------------------------------------------\n");
}

//--- Output Spool Change finished ------------------------------------
void print_spool_change_finished(void)
{
    clear_terminal();

    printf("+------------------------------------------------\n");
    printf("|   Flashforge 3D-Printer\n");
    printf("|\n");
    printf("|   Der Spulenwechsel ist abgeschlossen.\n");
    printf("|\n");
    printf("|   Druecken Sie Enter um ins Menu\n");
    printf("|   zurueckzukehren.\n");
    printf("|\n");
    printf("+------------------------------------------------\n");
}

//--- Output Spool Change abort ---------------------------------------
void print_spool_change_abort(void)
{
    printf("+------------------------------------------------\n");
    printf("|   Flashforge 3D-Printer\n");
    printf("|\n");
    printf("|   Der Spulenwechsel wurde abgebrochen.\n");
    printf("|\n");
    printf("+------------------------------------------------\n");
}

//--- Output No Directory ---------------------------------------------
void print_no_directory(void)
{
    printf("+------------------------------------------------\n");
    printf("|   Flashforge 3D-Printer\n");
    printf("|\n");
    printf("|   Das Verzeichnis existiert nicht.\n");
    printf("|\n");
    printf("|   Druecken Sie Enter um ins Menu\n");
    printf("|   zurueckzukehren.\n");
    printf("|\n");
    printf("+------------------------------------------------\n");
}

//--- Output File Error -----------------------------------------------
void print_error_file(void)
{
    printf("+------------------------------------------------\n");
    printf("|   Flashforge 3D-Printer\n");
    printf("|\n");
    printf("|   Die Datei konnte nicht geoeffnet werden.\n");
    printf("|\n");
    printf("|   Druecken Sie Enter um ins Menu\n");
    printf("|   zurueckzukehren.\n");
    printf("|\n");
    printf("+------------------------------------------------\n");
}

//--- Output Empty Directory ------------------------------------------
void print_empty_directory(void)
{
    printf("+------------------------------------------------\n");
    printf("|   Flashforge 3D-Printer\n");
    printf("|\n");
    printf("|   Das Verzeichnis beinhaltet keine\n");
    printf("|   druckbaren Datein.\n");
    printf("|\n");
    printf("|   Druecken Sie Enter um ins Menu\n");
    printf("|   zurueckzukehren.\n");
    printf("|\n");
    printf("+------------------------------------------------\n");
}

//--- Output File Search ----------------------------------------------
void print_file_search(void)
{
    clear_terminal();

    printf("+------------------------------------------------\n");
    printf("|   Flashforge 3D-Printer\n");
    printf("|\n");
    printf("|   File Suchen:\n");
    printf("|\n");
}

//--- Output filename from directory ----------------------------------
void print_directory_file(int file_number, char * filename)
{
    printf("| < %d >    %s\n", file_number, filename);
}

//--- Output file search end ------------------------------------------
void print_file_search_end(void)
{
    printf("|\n");
    printf("+------------------------------------------------\n");
}

//--- Output Printfile selection --------------------------------------
void print_select_file(void)
{
    printf("+------------------------------------------------\n");
    printf("|   Flashforge 3D-Printer\n");
    printf("|\n");
    printf("|   Waehlen Sie eine der aufgelisteten\n");
    printf("|   Druck-Dateien aus:\n");
    printf("|\n");
    printf("+------------------------------------------------\n");
}

//--- Output File selection aborted -----------------------------------
void print_select_file_abort(void)
{
    printf("+------------------------------------------------\n");
    printf("|   Flashforge 3D-Printer\n");
    printf("|\n");
    printf("|   Die Auswahl der Druckdatei wurde abgebrochen.\n");
    printf("|\n");
    printf("|   Druecken Sie Enter um ins Menu\n");
    printf("|   zurueckzukehren.\n");
    printf("|\n");
    printf("+------------------------------------------------\n");
}

//--- Output Printfile corrupted --------------------------------------
void print_printfile_corrupted(void)
{
    printf("+------------------------------------------------\n");
    printf("|   Flashforge 3D-Printer\n");
    printf("|\n");
    printf("|   Die Druckdatei ist nicht vollstaendig \n");
    printf("|   und kann nicht gedruckt werden.\n");
    printf("|\n");
    printf("|   Druecken Sie Enter um ins Menu\n");
    printf("|   zurueckzukehren.\n");
    printf("|\n");
    printf("+------------------------------------------------\n");
}

//--- Output Printfile is loaded --------------------------------------
void print_printfile_loaded(void)
{
    printf("+------------------------------------------------\n");
    printf("|   Flashforge 3D-Printer\n");
    printf("|\n");
    printf("|   Die ausgewaehlte Druckdatei  ist geladen.\n");
    printf("|\n");
    printf("+------------------------------------------------\n");
}

//--- Output Printfile not loaded -------------------------------------
void print_printfile_not_loaded(void)
{
    printf("+------------------------------------------------\n");
    printf("|   Flashforge 3D-Printer\n");
    printf("|\n");
    printf("|   Es ist keine Druckdatei geladen.\n");
    printf("|\n");
    printf("+------------------------------------------------\n");
}

//--- Output Start Print ----------------------------------------------
void print_start_print(void)
{
    printf("+------------------------------------------------\n");
    printf("|   Flashforge 3D-Printer\n");
    printf("|\n");
    printf("|   Der Druck wird gestartet.\n");
    printf("|\n");
    printf("+------------------------------------------------\n");
}

//--- Output finished heating -----------------------------------------
void print_heating_finished(int temperature)
{
    clear_terminal();

    printf("+------------------------------------------------\n");
    printf("|   Flashforge 3D-Printer\n");
    printf("|\n");
    printf("|   Das aufheizen auf %d Grad Celsius\n", temperature);
    printf("|   ist abgeschlossen.\n");
    printf("|\n");
    printf("+------------------------------------------------\n");
}

//--- Output estimated print time -------------------------------------
void print_estimated_print_time(int printtime_ms)
{
    int printtime_min = printtime_ms / (1000 * 60);     // calculate print time hours

    if(printtime_min > 60)                              // printtime higher than 60 min
    {
        int printtime_h = printtime_min / 60;           // calculate print time hours
        printtime_min = printtime_min % 60;             // calculate print time minutes

        printf("+------------------------------------------------\n");
        printf("|   Flashforge 3D-Printer\n");
        printf("|\n");
        printf("|   Die Druckzeit betraegt ca. %d h %d min.\n", printtime_h, printtime_min);
        printf("|\n");
        printf("+------------------------------------------------\n");
    }
    else
    {
        printf("+------------------------------------------------\n");
        printf("|   Flashforge 3D-Printer\n");
        printf("|\n");
        printf("|   Die Druckzeit betraegt ca. %d min.\n", printtime_min);
        printf("|\n");
        printf("+------------------------------------------------\n");
    }
}

//--- Output Spool turns clockwise (right) ----------------------------
void print_turn_spool_cw(void)
{
    printf("+------------------------------------------------\n");
    printf("|   Flashforge 3D-Printer\n");
    printf("|\n");
    printf("|   Die Spule dreht im Uhrzeigersinn.\n");
    printf("|\n");
    printf("+------------------------------------------------\n");
}

//--- Output start printing of shape ----------------------------------
void print_start_printing_shape(void)
{
    printf("+------------------------------------------------\n");
    printf("|   Flashforge 3D-Printer\n");
    printf("|\n");
    printf("|   Der Druck des Objektes hat\n");
    printf("|   gestartet:\n");
    printf("|\n");
    printf("+------------------------------------------------\n");
}

//--- Output Finished with the Print ----------------------------------
void print_finished_print(void)
{
    printf("+------------------------------------------------\n");
    printf("|   Flashforge 3D-Printer\n");
    printf("|\n");
    printf("|   Der Druck ist nun abgeschlossen.\n");
    printf("|\n");
    printf("|   Druecken Sie Enter um ins Menu\n");
    printf("|   zurueckzukehren.\n");
    printf("|\n");
    printf("+------------------------------------------------\n");
}

//--- Output continue to print ----------------------------------------
void print_continue_printing(void)
{
    printf("+------------------------------------------------\n");
    printf("|   Flashforge 3D-Printer\n");
    printf("|\n");
    printf("|   Moechten Sie dem Druck beginnen?\n");
    printf("|\n");
    printf("|   Wenn ja, bestaetigen Sie mit \'ok\'.\n");
    printf("|   Wenn nein, abbrechen mir \'a\'.\n");
    printf("|\n");
    printf("+------------------------------------------------\n");
}

// <-- end of file
